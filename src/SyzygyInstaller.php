<?php
namespace Syzygy\Installer;

use Composer\Installers\BaseInstaller;

/**
 * Installer for Syzygy skins.
 * @package Syzygy\Installer
 */
class SyzygyInstaller extends BaseInstaller
{
    /**
     * @var array
     */
    protected $locations = array(
        'skin' => 'wp-content/skins/{$name}/',
    );
}
